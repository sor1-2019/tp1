#include <pthread.h>
#include <stdio.h>
#include <semaphore.h>
#include <string.h>
#include <unistd.h>

int cant_clientes = 0; //cantidad de clientes esperando en la sala de espera
char* estado_barber; //estado del barbero, puede ser durmiendo o cortando
int n = 5; //capacidad de la sala de espera
pthread_mutex_t mutex;
sem_t cliente;
sem_t barber;

void *funcionBarber(){
	while (1) {
		pthread_mutex_lock(&mutex);  
		if(cant_clientes==0){
			//dormir
			estado_barber="durmiendo";
			printf("B: zzzZZZZZ\n");
			sem_wait(&cliente);
		}else{
			cant_clientes--;
			//cortar pelo
			estado_barber="cortando";
			printf("B: *cortando*\n");
			sleep(1);
			sem_post(&barber);
        	}
		pthread_mutex_unlock(&mutex);
	}
	pthread_exit(NULL);
}

void *funcionCliente(){
	if(cant_clientes < n){      
		//hay lugar, puedo entrar
		cant_clientes++;
		if(strcmp(estado_barber, "cortando") == 0){
			//esperar en la silla
			printf("C: *leyendo revista...*\n");
        		sem_wait(&barber);
		}else if (strcmp(estado_barber, "durmiendo") == 0){
			//despertar al barber
			printf("C: ejem, ejem.., cof, cof...\n");
			sem_post(&cliente);
		}
	}else{
		//no hay lugar, me voy
		printf("C: chau $ chau $...\n");
		sem_post(&cliente);
	}
	pthread_exit(NULL);
}

void * main (){
	pthread_t HILO1;
	pthread_t HILO2;  
	pthread_t HILO3;
	pthread_t HILO4;
	pthread_t HILO5;
	pthread_t HILO6;
	pthread_t HILO7;
	pthread_t HILO8;
	pthread_t HILO9;

	pthread_mutex_init(&mutex, NULL);
	sem_init(&barber, 0, 0);
	sem_init(&cliente, 0, 0);

	pthread_create(&HILO1, NULL, *funcionBarber,NULL);
	sleep(1);	
        pthread_create(&HILO2, NULL, *funcionCliente,NULL); 
        pthread_create(&HILO3, NULL, *funcionCliente,NULL);
	pthread_create(&HILO4, NULL, *funcionCliente,NULL); 
	pthread_create(&HILO5, NULL, *funcionCliente,NULL); 
	pthread_create(&HILO6, NULL, *funcionCliente,NULL);
	pthread_create(&HILO7, NULL, *funcionCliente,NULL); 
	pthread_create(&HILO8, NULL, *funcionCliente,NULL); 
	pthread_create(&HILO9, NULL, *funcionCliente,NULL); 

	pthread_join(HILO1 , NULL);
	pthread_join(HILO2 , NULL);
	pthread_join(HILO3 , NULL);
	pthread_join(HILO4 , NULL);
	pthread_join(HILO5 , NULL);
	pthread_join(HILO6 , NULL);
	pthread_join(HILO7 , NULL);
	pthread_join(HILO8 , NULL);
	pthread_join(HILO9 , NULL);

	pthread_mutex_destroy(&mutex);
	sem_destroy(&barber);
	sem_destroy(&cliente);

	pthread_exit(NULL);
}
