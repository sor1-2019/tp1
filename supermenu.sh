#!/bin/bash
#------------------------------------------------------
# PALETA DE COLORES
#------------------------------------------------------
#setaf para color de letras/setab: color de fondo
	red=`tput setaf 1`;
	green=`tput setaf 2`;
	blue=`tput setaf 4`;
	bg_blue=`tput setab 4`;
	reset=`tput sgr0`;
	bold=`tput setaf bold`;

#------------------------------------------------------
proyectoActual="/home/stormer/sor1/tp1" #path del proyecto

#------------------------------------------------------
# DISPLAY MENU
#------------------------------------------------------
imprimir_menu () {
       imprimir_encabezado "\t  S  U  P  E  R  -  M  E  N U ";
	
    echo -e "\t\t El proyecto actual es:";
    echo -e "\t\t $proyectoActual";
    
    echo -e "\t\t";
    echo -e "\t\t Opciones:";
    echo "";
    echo -e "\t\t\t a.  Ver estado del proyecto";
    echo -e "\t\t\t b.  Guardar cambios";
    echo -e "\t\t\t c.  Actualizar repo";
    echo -e "\t\t\t d.  Abrir en terminal";        
    echo -e "\t\t\t e.  Abrir en carpeta";
    echo -e "\t\t\t f.  Crear 99 carpetas";
    echo -e "\t\t\t g.  Eliminar 99 carpetas";
    echo -e "\t\t\t h.  Entrar al servidor de SOyR I con Putty";
    echo -e "\t\t\t i.  Abrir typing.com"
    echo -e "\t\t\t j.  Ver estado de un proceso"; 
    echo -e "\t\t\t k.  Abrir tablero de SOyR I en Trello";
    echo -e "\t\t\t l.  Interactuar con la funcion fork()";
    echo -e "\t\t\t m.  Ver arbol de procesos con n=3";
    echo -e "\t\t\t n.  Ejecutar ejercicio de la barbería";
    echo -e "\t\t\t o.  Ejecutar ejercicio del barbero de la opera";
    echo -e "\t\t\t p.  Descargar imagen de Debian";
    echo -e "\t\t\t q.  Descargar imagen de Debian con Axel";
    echo -e "\t\t\t r.  Provocar deadlock entre dos threads";
    echo -e "\t\t\t s.  Ejecutar ejercicio de semáforos en Python";
    echo -e "\t\t\t t.  Ejecutar ejercicio de depositos bancarios en Python";

    echo -e "\t\t\t x.  Salir";
    echo "";
    echo -e "Escriba la opción y presione ENTER";
}

#------------------------------------------------------
# FUNCTIONES AUXILIARES
#------------------------------------------------------

imprimir_encabezado () {
    clear;
    #Se le agrega formato a la fecha que muestra
    #Se agrega variable $USER para ver que usuario está ejecutando
    echo -e "`date +"%d-%m-%Y %T" `\t\t\t\t\t USERNAME:$USER";
    echo "";
    #Se agregan colores a encabezado
    echo -e "\t\t ${bg_blue} ${red} ${bold}--------------------------------------\t${reset}";
    echo -e "\t\t ${bold}${bg_blue}${red}$1\t\t${reset}";
    echo -e "\t\t ${bg_blue}${red} ${bold} --------------------------------------\t${reset}";
    echo "";
}

esperar () {
    echo "";
    echo -e "Presione enter para continuar";
    read ENTER ;
}

malaEleccion () {
    echo -e "Selección inválida..." ;
}

decidir () {
	echo $1;
	while true; do
		echo "¿Desea ejecutar? (s/n)";
    		read respuesta;
    		case $respuesta in
        		[Nn]* ) break;;
       			[Ss]* ) eval $1
				break;;
        		* ) echo "Por favor tipear S/s ó N/n.";;
    		esac
	done
}

#------------------------------------------------------
# FUNCTIONES del MENU
#------------------------------------------------------
a_funcion () {
    	imprimir_encabezado "\tOpción a.  Ver estado del proyecto";
	echo "---------------------------"        
	echo "¿Algo para realizar commit?"
        decidir "cd $proyectoActual; git status";

        echo "---------------------------"        
	echo "Cambios sin registrar, ¿necesita un pull?"
	decidir "cd $proyectoActual; git fetch origin"
	decidir "cd $proyectoActual; git log HEAD..origin/master --oneline"
}

b_funcion () {
       	imprimir_encabezado "\tOpción b.  Guardar cambios";
       	decidir "cd $proyectoActual; git add -A";
       	echo "Ingrese mensaje para el commit:";
       	read mensaje;
       	decidir "cd $proyectoActual; git commit -m \"$mensaje\"";
       	decidir "cd $proyectoActual; git push";
}

c_funcion () {
      	imprimir_encabezado "\tOpción c.  Actualizar repo";
      	decidir "cd $proyectoActual; git pull";   	 
}


d_funcion () {
	imprimir_encabezado "\tOpción d.  Abrir en terminal";        
	decidir "cd $proyectoActual; lxterminal &";
}

e_funcion () {
	imprimir_encabezado "\tOpción e.  Abrir en carpeta";        
	decidir "pcmanfm $proyectoActual &";
}

f_funcion () {
    	imprimir_encabezado "\tOpción f.  Crear 99 carpetas";
	echo "---------------------------"

	echo "Se crearán 99 carpetas en el directorio donde usted se encuentra.";
	esperar;        

	for i in {0..99}
	do
		printf "\tCreando carpeta: %s\n" "folder_$i"

		# Crear carpeta.
		mkdir folder_$i
	
		# Modificar permisos
		sudo chmod u+rw-x folder_$i
		sudo chmod g+r-wx folder_$i
		sudo chmod o-rwx folder_$i
	done
}

g_funcion () {
    	imprimir_encabezado "\tOpción g.  Eliminar 99 carpetas";
	echo "---------------------------"        

	echo "Se eliminarán las 99 carpetas creadas con la opción anterior.";
	esperar;

	for i in {0..99}		
	do
		printf "\tEliminando carpeta: %s\n" "folder_$i"

		# Eliminar carpeta.
		rmdir folder_$i
	done
}

h_funcion () {
	imprimir_encabezado "\tOpción h. Entrar al servidor de SOyR I con Putty";
	echo "---------------------------"

	echo "Para entrar al servidor, utilice los siguientes datos:"
	echo "IP: 192.168.1.11"
	echo "Usuario: user_x (x es su número de computadora en el laboratorio)"
	echo "Contraseña: sor1"
	esperar;
	putty
}

i_funcion () {
	imprimir_encabezado "\tOpción i. Abrir typing.com";
	echo "---------------------------"

	echo "Abriendo typing.com..."
	xdg-open https://www.typing.com/es/student/lessons
}

j_funcion () {
	imprimir_encabezado "\tOpción j. Ver estados de un proceso";
	echo "---------------------------"

	lxterminal -t top -e "top | grep 'ORDEN\| operaciones'"

	echo "Ejecutando el proceso 'operaciones', usted podra ver su estado en la nueva terminal abierta...";
	gcc operaciones.c -o operaciones
	chmod u+x operaciones
	./operaciones
}

k_funcion () {
	imprimir_encabezado "\tOpción k. Abrir tablero de SOyR I en Trello";
	echo "---------------------------"

	echo "Abriendo Trello..."
	xdg-open https://trello.com/b/63FSUNqW/sistemas-operativos-y-redes-i
}

l_funcion () {
	imprimir_encabezado "\tOpción l. Interactuar con la funcion fork()";
	echo "---------------------------"

	echo "Modifique el valor del entero n para conseguir diferentes resultados"
	esperar;

	nano fork.c
	gcc fork.c -o fork
	./fork
}

m_funcion () {
	imprimir_encabezado "\tOpción m. Ver el arbol de procesos con n=3";
	echo "---------------------------"

	xdg-open Fork.png
}

n_funcion () {
	imprimir_encabezado "\tOpción n. Ejercicio de la barbería";
	echo "---------------------------"

	gcc -pthread barberia.c -o barberia
	./barberia
}

o_funcion () {
	imprimir_encabezado "\tOpción o. Ejercicio del barbero de la opera";
	echo "---------------------------"

	echo "Modifique el valor del entero n para conseguir diferentes resultados"
	esperar;

	nano figaro.c
	gcc -pthread figaro.c -o figaro
	./figaro
}

p_funcion () {
	imprimir_encabezado "\tOpción p. Descargar imagen de Debian";
	echo "---------------------------"

	echo "Descargando archivo, al finalizar podrá ver la duración total de la descarga..."
	time wget https://cdimage.debian.org/debian-cd/current/i386/iso-cd/debian-10.1.0-i386-netinst.iso
}

q_funcion () {
	imprimir_encabezado "\tOpción q. Descargar imagen de Debian utilizando Axel";
	echo "---------------------------"

	echo "Descargando archivo con Axel, al finalizar podrá ver la duración total de la descarga..."
	time  axel -n 10 https://cdimage.debian.org/debian-cd/current/i386/iso-cd/debian-10.1.0-i386-netinst.iso
}

r_funcion () {
	imprimir_encabezado "\tOpción r. Provocar deadlocking entre dos threads";
	echo "---------------------------"
	
	echo "Se ejecutará el archivo 'deadlock', que produce un deadlocking entre un thread lector y un thread escritor";
	esperar;
	gcc -pthread deadlock.c -o deadlock
	./deadlock
}

s_funcion () {
	imprimir_encabezado "\tOpción s. Ejecutar ejercicio de semáforos en Python";
	echo "---------------------------"
	
	python helloworld.py
}

t_funcion () {
	imprimir_encabezado "\tOpción t. Ejecutar ejercicio de depositos bancarios en Python";
	echo "---------------------------"
	
	python depositos.py
}

#------------------------------------------------------
# LOGICA PRINCIPAL
#------------------------------------------------------
while  true
do
    # 1. mostrar el menu
    imprimir_menu;
    # 2. leer la opcion del usuario
    read opcion;
    
    case $opcion in
        a|A) a_funcion;;
        b|B) b_funcion;;
        c|C) c_funcion;;
        d|D) d_funcion;;
        e|E) e_funcion;;
        f|F) f_funcion;;
        g|G) g_funcion;;
	h|H) h_funcion;;
	i|I) i_funcion;;
	j|J) j_funcion;;
	k|K) k_funcion;;
	l|L) l_funcion;;
	m|M) m_funcion;;
	n|N) n_funcion;;
	o|O) o_funcion;;
	p|P) p_funcion;;
        q|Q) q_funcion;;
	r|R) r_funcion;;
	s|S) s_funcion;;
	t|T) t_funcion;;
        x|X) break;;
	*) malaEleccion;;
    esac
    esperar;
done
 
