import threading, time
sem = threading.Semaphore()
n = int(input("Ingrese la cantidad de veces que quiere que se imprima la cadena. \n"))

def funcionHello():
	for i in range (0, n):	        
		sem.acquire()
		print("Hello")
		sem.release()
		time.sleep(0.25)

def funcionWorld():
	for i in range (0, n):
		sem.acquire()
		print("world")
		sem.release()
		time.sleep(0.25)

hilo1 = threading.Thread(target = funcionHello)
hilo1.start()
hilo2 = threading.Thread(target = funcionWorld)
hilo2.start()
