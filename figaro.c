#include <stdio.h>
#include <stdlib.h>  
#include <pthread.h>
#include <semaphore.h>

int n = 5;
int contador = 0;
int cantFigaro = 0;
sem_t fiiiigaro;
sem_t figaro;
sem_t figaroFi;
sem_t figaroFa;

void *funcionFiiiigaro(){
	while(1){
	   contador++;
	   sem_wait(&fiiiigaro);
	   printf("Fiiiigaro\n");
	   sem_post(&figaro);
	}
}

void *funcionFigaro(){
	while(1){
		sem_wait(&figaro);
		if(cantFigaro < 2){
			printf("Figaro ");
			cantFigaro++;
			sem_post(&figaro);
		} else {
	   		printf("Figaro\n");
			cantFigaro = 0;
	   		sem_post(&figaroFi);
		}
	}
}

void *funcionFigaroFi(){
	while(1){
	   sem_wait(&figaroFi);
	   printf("Figaro Fi\n");
	   sem_post(&figaroFa);
	}
}

void *funcionFigaroFa(){
	while(1){
	   sem_wait(&figaroFa);
	   printf("Figaro Fa\n");
	   sem_post(&fiiiigaro);
	   if(contador == n){
		exit(-1);
	   }
	}	
}

void * main (){

	sem_init(&fiiiigaro,0,1);
	sem_init(&figaro,0,0);
	sem_init(&figaroFi,0,0);
	sem_init(&figaroFa,0,0);

	pthread_t HILO1;
	pthread_t HILO2;
	pthread_t HILO3;
	pthread_t HILO4;
	pthread_t HILO5;
	pthread_t HILO6;
		
	pthread_create(&HILO1, NULL, *funcionFiiiigaro,NULL); 
	pthread_create(&HILO2, NULL, *funcionFigaro,NULL);
	pthread_create(&HILO3, NULL, *funcionFigaro,NULL);
	pthread_create(&HILO4, NULL, *funcionFigaro,NULL);
	pthread_create(&HILO5, NULL, *funcionFigaroFi,NULL);
	pthread_create(&HILO6, NULL, *funcionFigaroFa,NULL);

	pthread_join(HILO1 , NULL);
	pthread_join(HILO2 , NULL);
	pthread_join(HILO3 , NULL);
	pthread_join(HILO4 , NULL);
	pthread_join(HILO5 , NULL);
	pthread_join(HILO6 , NULL);

	//destruir los semaforos
	sem_destroy(&fiiiigaro);
	sem_destroy(&figaro);
	sem_destroy(&figaroFi);
	sem_destroy(&figaroFa);

	pthread_exit(NULL);
}
