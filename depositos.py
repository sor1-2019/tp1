import threading

mutex = threading.Lock()
saldo = 1000

def funcionDepositar(monto):
	global mutex
	global saldo
	mutex.acquire()
	aux = saldo
	aux = aux + monto
	saldo = aux
	mutex.release()
	print "El saldo de la cuenta es: ", saldo

print "El saldo de la cuenta es: ", saldo
hilo1 = threading.Thread(target = funcionDepositar(monto = int(input("Monto a depositar: "))))
hilo2 = threading.Thread(target = funcionDepositar(monto = int(input("Monto a depositar: "))))
hilo1.start()
hilo2.start()
