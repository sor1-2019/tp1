#include <stdio.h>
#include <limits.h>

int main() {
   
	FILE *puntero;

	puntero = fopen("/tmp/proceso.log", "w+");

	int i_max = INT_MAX/100;

	while(1){
		printf("Esperando...\n Presione cualquier tecla...");

		//esperar a que una tecla sea presionada   	 
		getchar();
   	 
		printf("Realizando cálculos...\n\n");
   	 
		//ciclo donde el proceso hace operaciones aritmeticas
		int contador = 0;

		while(contador < i_max){
			contador = contador + 1;
   		    		 
			//escribo la variable contador en puntero   		 
			fprintf(puntero, "Linea...%d \n", contador);
		}
	}
	fclose(puntero);
	return 0; 
}

