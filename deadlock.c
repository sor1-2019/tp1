#include <stdio.h>
#include <pthread.h>
#include <stdlib.h>
#include <semaphore.h>
#include <unistd.h>

pthread_mutex_t lector;
pthread_mutex_t escritor;
FILE *fp;
int cont = 0;

void * escribir(void * temp){
	while(1){
		pthread_mutex_lock(&escritor);
		sleep(5);
		pthread_mutex_lock(&lector);
		printf("\nArchivo bloqueado");
		fp = fopen("/home/stormer/sor1/tp1/datos.txt", "w");
		cont++;		
		fprintf(fp, "%s", "Linea %dcont");
		fclose(fp);
		pthread_mutex_unlock(&lector);
		pthread_mutex_unlock(&escritor);
		printf("\nArchivo desbloqueado, ahora puedes leerlo");
	}
	pthread_exit(NULL);
}

void * leer(void * temp){
	while(1){
		char *str;
		pthread_mutex_lock(&lector);
		sleep(5);
		pthread_mutex_lock(&escritor);
		printf("\nAbriendo archivo.");
		fp = fopen("/home/stormer/sor1/tp1/datos.txt", "r");
		str = (char*) malloc(10 * sizeof(char));
		fscanf(fp, "%s", str);
		printf("\nEl archivo dice %s \n", str);
		fclose(fp);
		pthread_mutex_unlock(&escritor);
		pthread_mutex_unlock(&lector);
	}
	pthread_exit(NULL);
}

void * main(){
	pthread_mutex_init(&lector, NULL);
	pthread_mutex_init(&escritor, NULL);

	pthread_t hiloEscritor;
	pthread_t hiloLector;

	pthread_create(&hiloEscritor, NULL, *escribir,NULL); 
	pthread_create(&hiloLector, NULL, *leer,NULL);

	pthread_join(hiloEscritor, NULL);
	pthread_join(hiloLector, NULL);

	pthread_mutex_destroy(&lector);
	pthread_mutex_destroy(&escritor);
	pthread_exit(NULL);
}
